@file:Suppress("PackageDirectoryMismatch")

package org.example.weather

import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import kotlinx.cinterop.*
import platform.posix.fclose
import platform.posix.fopen
import platform.posix.getline

actual typealias HttpClient = HttpClient

internal actual fun fetchApiKey(): String = memScoped {
    val readSize = alloc<ULongVar>()
    val line = allocArray<CPointerVar<ByteVar>>(500)
    val result: String
    val readMode = "r"
    val filePtr = fopen("api_key.txt", readMode)
    getline(__stream = filePtr, __lineptr = line, __n = readSize.ptr)
    result = line[0]?.toKString()?.replace("\n", "") ?: ""
    fclose(filePtr)
    result
}

internal actual fun createHttpClient() = HttpClient {
    install(JsonFeature) {
        // Use KotlinxSerializer(Json.nonstrict) when some JSON fields need to be skipped.
        serializer = KotlinxSerializer()
    }
}
