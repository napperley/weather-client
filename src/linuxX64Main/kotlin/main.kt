package org.example.weather

import io.ktor.client.request.get
import kotlinx.coroutines.runBlocking

private const val LATITUDE = -43.5321
private const val LONGITUDE = 172.6362

fun main() = runBlocking {
    println("Starting weather...")
    val apiKey = fetchApiKey()
    val url = createCurrentWeatherUrl(apiKey = apiKey, latitude = LATITUDE, longitude = LONGITUDE)
    println("API Key: $apiKey")
    val client = createHttpClient()
    println("Fetching current weather via OpenWeather REST API...")
    val resp = client.get<CurrentWeather>(url)
    printCurrentWeather(resp)
    println("Exiting...")
    client.close()
}
