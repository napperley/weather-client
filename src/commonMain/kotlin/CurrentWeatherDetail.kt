package org.example.weather

import kotlinx.serialization.Serializable

@Serializable
data class CurrentWeatherDetail(
    val temp: Double,
    val feels_like: Double,
    val temp_min: Double,
    val temp_max: Double,
    val pressure: Int,
    val humidity: Int
)
