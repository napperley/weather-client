package org.example.weather

import kotlinx.serialization.Serializable

@Serializable
data class WeatherItem(val id: Int, val main: String, val description: String, val icon: String)
