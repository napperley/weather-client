package org.example.weather

import kotlinx.serialization.Serializable

@Serializable
data class CurrentWeather(
    val name: String,
    val id: Long,
    val timezone: Int,
    val base: String,
    val weather: List<WeatherItem>,
    val cod: Int,
    val main: CurrentWeatherDetail,
    val wind: Wind,
    val clouds: Cloud,
    val dt: Long,
    val sys: WeatherSystem,
    val coord: Location,
    val visibility: Long
)
