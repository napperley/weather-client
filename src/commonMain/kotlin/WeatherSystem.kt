package org.example.weather

import kotlinx.serialization.Serializable

@Serializable
data class WeatherSystem(
    val type: Int,
    val id: Long,
    val message: Double = 0.0,
    val country: String,
    val sunrise: Long,
    val sunset: Long
)
