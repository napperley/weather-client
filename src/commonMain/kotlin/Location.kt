package org.example.weather

import kotlinx.serialization.Serializable

@Serializable
data class Location(val lon: Double, val lat: Double)
