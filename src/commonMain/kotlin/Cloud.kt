package org.example.weather

import kotlinx.serialization.Serializable

@Serializable
data class Cloud(val all: Int)
