package org.example.weather

expect class HttpClient

private const val BASE_URL = "https://api.openweathermap.org/data/2.5/weather"

internal expect fun fetchApiKey(): String

fun printCurrentWeather(weather: CurrentWeather) {
    println("-- Current Weather --")
    println("Timezone: ${weather.timezone}")
    println("Place: ${weather.name}")
    println("Temperature: ${weather.main.temp}")
    println("Min Temperature: ${weather.main.temp_min}")
    println("Max Temperature: ${weather.main.temp_max}")
    println("Wind Pressure: ${weather.main.pressure}")
    if (weather.weather.isNotEmpty()) {
        with(weather.weather.first()) {
            println("Description: $description")
        }
    }
}

internal expect fun createHttpClient(): HttpClient

internal fun createCurrentWeatherUrl(apiKey: String, latitude: Double, longitude: Double) =
    "$BASE_URL?APPID=$apiKey&lat=$latitude&lon=$longitude&units=metric"
