package org.example.weather

import kotlinx.serialization.Serializable

@Serializable
data class Wind(val speed: Double, val deg: Double)
