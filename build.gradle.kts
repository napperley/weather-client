group = "org.example"
version = "1.0-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.3.61"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.3.61"
}

repositories {
    jcenter()
    mavenCentral()
}

kotlin {
    linuxX64 {
        compilations.getByName("main") {
            dependencies {
                val ktorClientVer = "1.3.0"
                implementation("io.ktor:ktor-client-curl:$ktorClientVer")
                implementation("io.ktor:ktor-client-serialization-native:$ktorClientVer")
            }
        }
        binaries {
            executable("weather") {
                entryPoint = "org.example.weather.main"
            }
        }
    }
    sourceSets {
        getByName("commonMain") {
            dependencies {
                val kotlinVer = "1.3.61"
                val serializationVer = "0.14.0"
                implementation(kotlin("stdlib-common", kotlinVer))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:" +
                    serializationVer)
            }
        }
        getByName("linuxX64Main") {
            languageSettings.useExperimentalAnnotation("kotlin.Experimental")
        }
    }
}
